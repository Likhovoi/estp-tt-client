// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import transactions from './transactions';

const rootReducer = combineReducers({
  transactions,
  router,
});

export default rootReducer;
