// @flow
import { LOAD_TRANSACTIONS, FAILED } from '../actions/transactions';

export type Transaction = {
    +value: number;
    +currentValue: number;
    +createdAt: number;
}

export type actionType = {
  +type: string,
  +transactions?: Transaction[],
  +error?: Error,
};

type stateType = {
    list: null|Transaction[];
    failed: boolean;
};

const initialState = {
    list: null,
    failed: false,
};

export default function transactions(state: stateType = initialState, action: actionType): stateType {
  switch (action.type) {
    case LOAD_TRANSACTIONS:
        return {
            failed: false,
            list: action.transactions || null,
        };
    case FAILED:
        return {
            failed: true,
            list: null,
        };
    default:
      return state;
  }
}
