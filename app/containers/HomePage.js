// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Home from '../components/Home';
import { bindActionCreators } from 'redux';
import * as TransactionsActions from '../actions/transactions';

type Props = {};

function mapStateToProps(state) {
  return {
    transactions: state.transactions.list,
    failed: state.transactions.failed,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(TransactionsActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);