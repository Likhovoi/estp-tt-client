// @flow
import axios from 'axios';
import type { actionType } from '../reducers/transactions';

export const LOAD_TRANSACTIONS = 'LOAD_TRANSACTIONS';
export const FAILED = 'FAILED';

export function fetch() {
    const apiHost: string = process.env.API_HOST || '';
    return async (dispatch: (action: actionType) => void) => {
        try {
            const response = await axios.get(apiHost + '/history');
            dispatch({
                type: LOAD_TRANSACTIONS,
                transactions: response.data,
            });
        } catch (error) {
            dispatch({
                type: FAILED,
                error,
            });
        }
    }
}
