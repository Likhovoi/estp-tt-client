// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes.json';
import styles from './Home.css';
import { Transaction } from 'electron';

type Props = {
  fetch: () => void;
  transactions: Transaction[]|null;
  failed: boolean;
};

/**
 * There is no grid in requiremenets!
 */
export default class Home extends Component<Props> {
  props: Props;

  timestampToString(timestamp: number) {
    // TODO: use recompose
    const date = new Date();
    date.setTime(timestamp);
    return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
  }

  render() {
    if (this.props.failed) {
      return (
        <div className={styles['server-error']}>
          Server respond with error
        </div>
      );
    }

    if (this.props.transactions === null) {
      this.props.fetch();
      return ''; // TODO: add loading
    }

    return (
      <div data-tid="container">
        <div className={styles['transactions-list-header']}>
          <h2>Transactions history list</h2>
        </div>
        <div className={styles['transactions-list-body']} id="accordion">
            { this.props.transactions.slice().reverse().map(txn => (
              <div className={'card ' + styles['card']} key={txn.createdAt}>
                <div className={'card-header ' + styles['card-header']} id={'txnHeader' + txn.createdAt} data-toggle="collapse" data-target={'#txnCollapse' + txn.createdAt}>
                  <button className={"btn btn-link collapsed " + (txn.value < 0 ? styles['card-btn-credit'] : styles['card-btn-debit'])} aria-expanded="false" aria-controls={'txnCollapse' + txn.createdAt}>
                    {txn.value.toFixed(2)}
                  </button>
                </div>
                <div id={'txnCollapse' + txn.createdAt} className="collapse" aria-labelledby={'txnHeader' + txn.createdAt} data-parent="#accordion">
                  <div className="card-body" style={{ color: 'black' }}>
                    <p>Date: { this.timestampToString(txn.createdAt) }</p>
                    <p>Value: { txn.value }</p>
                    <p>Current Value: { txn.currentValue }</p>
                  </div>
                </div>
              </div>
            )) }
        </div>
      </div>
    );
  }
}
